# SPDX-FileCopyrightText: 2020, juga <juga at riseup dot net>
# SPDX-License-Identifier: AGPLv3
"""pEp Message unit tests."""
from . import constants


def test_msg_len_changes(create_alice_identity, create_bob_identity):
    """
    Test that the original message is modified after encryption.
    Headers are added and therefore the modified unencrypted message length
    is different to the original.

    Author: juga at riseup dot net

    """
    import pEp

    alice = create_alice_identity
    bob = create_bob_identity

    msg = pEp.outgoing_message(alice)
    msg.to = [bob]
    msg.shortmsg = constants.SUBJECT
    msg.longmsg = constants.BODY
    msg_len = len(str(msg))
    # Encrypt Message
    msg.encrypt()

    # After encryption, the original message is modified!!
    # It contains one more header and the alice's public key, if it's the first
    # msg to bob.
    msg_after_encrypt_len = len(str(msg))
    assert msg.shortmsg != constants.SUBJECT
    assert msg.longmsg == constants.BODY
    assert msg_after_encrypt_len != msg_len


def test_dec_msg_len(create_alice_identity, create_bob_identity):
    """
    Test that the decrypted message length is different from the original.

    Author: juga at riseup dot net

    """
    import pEp

    alice = create_alice_identity
    bob = create_bob_identity

    msg = pEp.outgoing_message(alice)
    msg.to = [bob]
    msg.shortmsg = constants.SUBJECT
    msg.longmsg = constants.BODY
    msg_len = len(str(msg))
    # Encrypt Message
    enc_msg = msg.encrypt()

    # Decrypt message.
    dec_msg, _key_list, _rating, _r = enc_msg.decrypt()
    dec_msg_len = len(str(dec_msg))

    assert dec_msg.longmsg.replace("\r", "") == constants.BODY  # msg.longmsg
    expected_dec_msg = """From: Alice Lovelace <alice@openpgp.example>\r
To: Bob Babagge <bob@openpgp.example>\r
Subject: This is a subject\r
X-pEp-Version: 2.1\r
X-EncStatus: reliable\r
X-KeyList: \r
 EB85BB5FA33A75E15E944E63F231550C4F47E38E,EB85BB5FA33A75E15E944E63F231550C4F47E38E,D1A66E1A23B182C9980F788CFBFCC82A015E7330\r
MIME-Version: 1.0\r
Content-Type: text/plain\r
Content-Transfer-Encoding: 7bit\r
\r
Hi world!\r
"""
    assert expected_dec_msg == str(dec_msg)
    # The decrypted message length should then be equal to the original message
    # minus the extra headers added.
    dec_lines = str(dec_msg).split("\n")
    extra_headers_lines = dec_lines[3:7]
    extra_headers = "\n".join(extra_headers_lines) + "\n"
    len_extra_headers = len(extra_headers)
    print("len_extra_headers", len_extra_headers)
    assert dec_msg_len - len_extra_headers == msg_len


def test_null_char_rmed(create_alice_identity, create_bob_identity):
    """
    Test that null characters and anything after them is removed.

    Author: juga at riseup dot net

    """
    import pEp

    alice = create_alice_identity
    bob = create_bob_identity

    msg = pEp.outgoing_message(alice)
    msg.to = [bob]
    msg.shortmsg = constants.SUBJECT

    # Message with null chars, potentially for padding.
    body = 'Hi Bob,\n' + '\0' * 255 + '\nBye,\nAlice.'
    msg.longmsg = body
    # XXX: The null characters and anything after them is removed.
    # Change this assertion when this is solved.
    assert msg.longmsg != body
