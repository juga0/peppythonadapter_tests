
This repository is temporal and it only exists until
https://pep.foundation/dev/repos/pEpPythonAdapter/ migrates to git
and uses some Software Configuration Management tool that allows Pull Requests
or Merge Requests other than sending an Email to an Email list that maintainers
might not be in.

Tor run the tests, install
[pEpPythonAdapter](https://pep.foundation/dev/repos/pEpPythonAdapter/) and
[pytest](https://pytest.org/) and run `pytest`.
